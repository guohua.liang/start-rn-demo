
# demo

## 如何起服务

step 1 将host项目和业务项目安装在同一个文件夹内，并且重命名为host和app

```shell
git clone https://git.garena.com/weiping.xiang/none-shopee-demo.git app
git clone https://git.garena.com/weiping.xiang/none-shopee-demo-host.git host
```

step 2 在业务项目里面安装依赖`yarn`，执行`yarn run init`将软链接建立在host项目当中，并且帮host安装依赖

step 3 `yarn dev`起服务，模拟器连接ip端口，进行reload。

## 如何进行打包

step 1 进入host项目，清理掉依赖`rm -rf node_modules`, 重新安装依赖`yarn`

step 2 执行命令 `npx cpp-cli rn-centralized-package --rnVersion 1 --region id`。其中参数是用于热更新系统的，暂时可以忽略，在正式接入的时候，需要调整。

安卓打包产物在dist/dest/1/all.android.zip当中，安卓分为1x，2x, 3x，all则是包含所有图片尺寸

## 注意

中心化构建是通过npm的git地址来维护这个关系的。

Deploy Token是在业务项目的gitlab =》Setting =》Repository =》 Deploy Tokens中设置

```javascript
{
 "@shopee-rn/app": "git+https://gitlab+deploy-token-331:LNstkHAuAsrXmPFSyvM4@git.garena.com/weiping.xiang/none-shopee-demo.git#dc7119d2"
}
```

## 安装adb

> 参考https://www.e-learn.cn/content/qita/1005452

踩坑1：source .bash_profile在其他终端失效

解决：https://www.jianshu.com/p/cf84c5dfe1cd

## 安装Android Studio

```bash
# https://developer.android.google.cn/studio/archive
# 版本选择4.0.2，版本太旧或者太新可能会没有AVD Manager的工具～
```

## 创建一个模拟器

- 打开Android studio

- 选择 + start a new ..。下次再打开则可以在左侧列表选择上次打开项目

![image-20210208191607621](/img/image-20210208191607621.png)

- 完成后，选择Tool > AVD Manager，点击 + Create Virtual Device

![](/img/image-20210208182353740.png)

![image-20210208182447071](/img/image-20210208182447071.png)

- 选择一个分辨率

![image-20210208182538348](/img/image-20210208182538348.png)

- 选择一个Android版本，点击Download（一直下一步，等待安装完成）

![image-20210208182622550](/img/image-20210208182622550.png)

- 回到Tool > AVD Manager，列表中多了一个机型，点击右侧▶️ 开启此模拟器

![image-20210208182747269](/img/image-20210208182747269.png)

- 接入adb安装apk到此模拟器

```bash
# 先确保adb服务已经运行
adb start-server
# 查看模拟器状态，状态attached为device则表示模拟器状态正常
adb devices
# 将apk安装到模拟器中。确保模拟器已经允许安装应用，切换到模拟器界面点击允许安装（首次安装）
adb install 你的apk所在绝对路径 
```

- 完成

## 拉取安卓团队的项目

```bash
git clone ...
```

-  打开Android Studio，点击Open an existing...导入
